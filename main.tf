provider "aws" {
    region = "ap-southeast-1"
    access_key = "AKIA3G2AG7TP2RN3ECWJ"
    secret_key = "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" 
}


variable "vpc_cidr_block" {
    description = "vpc cidr block"
}

variable "subnet_cidr_block" {
    description = "subnet cidr block"
}

resource "aws_vpc" "developement-vpc" {
    cidr_block = var.vpc_cidr_block
        tags = {
        Name = "development"
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.developement-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = "ap-southeast-1a"
        tags = {
        Name = "dev-subnet-1"
    }
}

data "aws_vpc" "existing-vpc" {
    default = true
}

resource "aws_subnet" "dev-subnet-2" {
    vpc_id = data.aws_vpc.existing-vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = "ap-southeast-1a"
     tags = {
        Name = "dev-subnet-2"
    }
}

output "dev-vpc-id" {
    value = aws_vpc.developement-vpc.id
}

output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
}



